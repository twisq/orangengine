import React, { useState } from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import CircularProgress from "@material-ui/core/CircularProgress";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import AccountCircle from '@material-ui/icons/AccountCircle';

import { User } from "./App";

import config from "./config.json";

type onSetUserHandler = (user: User) => void;

interface LoginProperties {
  user: User;
  onSetUser: onSetUserHandler;
}

const useStyles = makeStyles((theme: Theme) => ({
  wrong: {
    display: "block",
    margin: theme.spacing(2),
    color: "red"
  },
  login: {
    display: "block",
    margin: theme.spacing(2),
  },
  loginButton: {
    color: "white",
    margin: theme.spacing(1)
  },
  textinput: {
    display: "block",
    margin: theme.spacing(1),
    width: 400
  },
  buttoninput: {
    margin: theme.spacing(1)
  },
  accountCircle: {
    marginRight: theme.spacing(1)
  }
}));


function Login(props: LoginProperties) {

  const classes = useStyles();

  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [dialogOpen, setdialogOpen] = useState<boolean>(false);
  const [busy, setBusy] = useState<boolean>(false);
  const [wrong, setWrong] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  
  const handleLoginButton = () => {
    setdialogOpen(true);
  }

  const handleCancelButton = () => {
    setdialogOpen(false);
    setBusy(false);
    setWrong(false);
    setError(false);
  }

  // Handle keypresses during login phase.
  const handleUsernameInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setUsername(event.target.value);
  };
  const handlePasswordInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setPassword(event.target.value);
  };

  // User has entered her credentials. Test if they are valid.
  const handleLogin = () => {
    setBusy(true);
    setError(false);
    setWrong(false);
    const headers = ({
      "Accept": "application/json",
      "Content-type": "application/json"
    });
    const contents = ({
      "username": username,
      "password": password
    });
    fetch(config.urlLogin, {
        mode: "cors",
        headers: headers,
        method: "post",
        cache: "no-cache",
        body: JSON.stringify(contents)
    })
    .then(result => {
        if (result.status === 200) {
            return result.json();
        } else if (result.status === 401) {
            setWrong(true);
            return Promise.reject();
        } else {
            console.log(result);
            throw new Error("REST service not okay. " + result.status + ": " + result.statusText);
        }
    })
    .then(json => {
        props.onSetUser({
          username: username,
          realname: json.realname,
          role: json.role,
          token:  json.token,
          tokenexpirytime: new Date(json.tokenexpirytime)
        });
        setdialogOpen(false);
    })
    .catch(error => {
        console.log(error);
        if (error !== undefined) setError(true);
    })
    .finally(() => {
      setBusy(false);
    });
  };

  // Catch enter keypress.
  const handleKeyPress = (event: React.KeyboardEvent) => {
    if ((event.key === "Enter") && (username !== "") && (password !== "")) {
      handleLogin();
    }
  }

  return (
    <div>

        {
        (props.user.token === null) && (
          <Button className={classes.loginButton} variant="outlined" onClick={handleLoginButton}>Login</Button>
        )}

        {
        (props.user.token !== null) && (
          <Typography variant="h6">
            <AccountCircle className={classes.accountCircle}/>
            {props.user.realname}
          </Typography>
        )}

        <Dialog open={dialogOpen} aria-labelledby="form-dialog-title" onClose={handleCancelButton} onKeyPress={handleKeyPress}>
          <DialogTitle id="form-dialog-title">Login</DialogTitle>
          <DialogContent>
            <DialogContentText>Geef je naam en wachtwoord op:</DialogContentText>

                <TextField
                  className={classes.textinput}
                  id="username"
                  label="Gebruikersnaam"
                  variant="outlined"
                  fullWidth
                  autoFocus
                  autoComplete="off"
                  disabled={busy}
                  onChange={handleUsernameInputChange}
                />
                <TextField
                  className={classes.textinput}
                  id="password"
                  label="Wachtwoord"
                  type="password"
                  variant="outlined"
                  fullWidth
                  disabled={busy}
                  onChange={handlePasswordInputChange}
                />
                        
                {busy && (
                   <div style={{display: 'flex', justifyContent: 'center', marginTop: 100}}>
                      <CircularProgress />
                  </div>
                )}

                {(wrong) && (
                  <p className={classes.wrong}>De combinatie van de ingevoerde naam en wachtwoord is niet juist.</p>
                )}

                {(error) && (
                  <p className={classes.wrong}>Er is iets mis met de server waar dit programma op draait. Probeer het later nog eens.</p>
                )}
                
              </DialogContent>
              <DialogActions>
                <Button
                    className={classes.buttoninput}
                    color="secondary"
                    variant="contained"
                    onClick={handleCancelButton}>
                      Annuleer
                  </Button>
                  <Button
                    className={classes.buttoninput}
                    color="secondary"
                    variant="contained"
                    disabled={(username === "") || (password === "")}
                    onClick={handleLogin}>
                      Login
                  </Button>
                </DialogActions>
        </Dialog>
      </div>
  );
}

export default Login;