import React, { useState } from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { User } from "./App";

import config from "./config.json";

type onChangedHandler = () => void;

interface AddIdeaProperties {
  user: User;
  onChanged: onChangedHandler;
}

const useStyles = makeStyles((theme: Theme) => ({
  error: {
    display: "block",
    margin: theme.spacing(2),
    color: theme.palette.error.main
  },
  addButton: {
    color: "white",
    margin: theme.spacing(1)
  },
  textinput: {
    display: "block",
    margin: theme.spacing(1),
    width: 800
  },
  buttoninput: {
    margin: theme.spacing(1)
  },
  accountCircle: {
    marginRight: theme.spacing(1)
  }
}));


function AddIdea(props: AddIdeaProperties) {

  const classes = useStyles();

  const [name, setName] = useState<string>("");
  const [comments, setComments] = useState<string>("");
  const [idea, setIdea] = useState<string>("");
  // eslint-disable-next-line
  const [platform, setPlatform] = useState<string>("IOTA Devnet");
  const [dialogOpen, setdialogOpen] = useState<boolean>(false);
  const [busy, setBusy] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  
  const isInputValid = () : boolean => {
    return ((name !== "") && (comments !== "") && (idea !== ""));
  }

  const handleAddIdeaButton = () => {
    setdialogOpen(true);
  }

  const handleCancelButton = () => {
    setdialogOpen(false);
    setBusy(false);
    setError(false);
  }

  // Handle keypresses.
  const handleNameInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setName(event.target.value);
  };
  const handleCommentsInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setComments(event.target.value);
  };
  const handleIdeaInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setIdea(event.target.value);
  };

  // User has entered her credentials. Test if they are valid.
  const handleAdd = () => {
    setBusy(true);
    setError(false);
    const headers = ({
      "Accept": "application/json",
      "Content-type": "application/json"
    });
    const contents = ({
      "token": props.user.token,
      "name": name,
      "comments": comments,
      "idea": idea,
      "platform": platform
    });
    fetch(config.urlAddIdea, {
        mode: "cors",
        headers: headers,
        method: "post",
        cache: "no-cache",
        body: JSON.stringify(contents)
    })
    .then(result => {
        if (result.status === 200) {
            return result.json();
        } else if (result.status === 401) {
            return Promise.reject();
        } else {
            console.log(result);
            throw new Error("REST service not okay. " + result.status + ": " + result.statusText);
        }
    })
    .then(json => {
        setdialogOpen(false);
        props.onChanged();
    })
    .catch(error => {
        console.log(error);
        if (error !== undefined) setError(true);
    })
    .finally(() => {
      setBusy(false);
    });
  };

  return (
    <div>

        <Button 
            className={classes.addButton} 
            color="secondary" 
            variant="contained"
            disabled={props.user.token === null} 
            onClick={handleAddIdeaButton}
        >Nieuw idee</Button>

        <Dialog 
            open={dialogOpen}
            maxWidth={false}
            aria-labelledby="form-dialog-title" 
            onClose={handleCancelButton}
        >
          <DialogTitle id="form-dialog-title">Nieuw idee</DialogTitle>
          <DialogContent>
            <DialogContentText>Vul hier de gegevens in:</DialogContentText>

                <TextField
                  className={classes.textinput}
                  id="name"
                  label="Naam van idee"
                  variant="outlined"
                  fullWidth
                  autoFocus
                  autoComplete="off"
                  disabled={busy}
                  onChange={handleNameInputChange}
                />
                <TextField
                  className={classes.textinput}
                  id="comments"
                  label="Opmerkingen"
                  variant="outlined"
                  fullWidth
                  autoComplete="off"
                  disabled={busy}
                  onChange={handleCommentsInputChange}
                />
                <TextField
                  className={classes.textinput}
                  id="idea"
                  label="Beschrijving idee"
                  variant="outlined"
                  fullWidth
                  multiline
                  rows={16}
                  autoComplete="off"
                  disabled={busy}
                  onChange={handleIdeaInputChange}
                />
                        
                {busy && (
                   <div style={{display: 'flex', justifyContent: 'center', marginTop: 100}}>
                      <CircularProgress />
                  </div>
                )}

                {(error) && (
                  <p className={classes.error}>Er is iets mis met de server waar dit programma op draait. Probeer het later nog eens.</p>
                )}
                
              </DialogContent>
              <DialogActions>
                <Button
                    className={classes.buttoninput}
                    color="secondary"
                    variant="contained"
                    onClick={handleCancelButton}>
                      Annuleer
                  </Button>
                  <Button
                    className={classes.buttoninput}
                    color="secondary"
                    variant="contained"
                    disabled={!isInputValid()}
                    onClick={handleAdd}>
                      Vastleggen
                  </Button>
                </DialogActions>
        </Dialog>
      </div>
  );
}

export default AddIdea;