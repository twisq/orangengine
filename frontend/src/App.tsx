import React, { useState } from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Login from "./Login";
import Ideas from "./Ideas";
import About from "./About";

import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles((theme: Theme) => ({
  error: {
    display: "block",
    margin: theme.spacing(2),
    color: theme.palette.error.main
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: "none",
    flexGrow: 1,
    [theme.breakpoints.up("sm")]: {
      display: "block",
    }
  },
  user: {
    margin: theme.spacing(1)
  }
}));

// Administration of currently logged in user.
export interface User {
  username: string;
  realname: string;
  token: string | null;
  role: string;
  tokenexpirytime: Date;
};

function App() {

  const classes = useStyles();

  const [user, setUser] = useState<User>({
    username: "",
    realname: "",
    token: null,
    role: "",
    tokenexpirytime: new Date(),
  });

  const [error, setError] = useState<string | null>(null);
  const [aboutOpen, setAboutOpen] = useState<boolean>(false);

  const openAbout = () => {
    setAboutOpen(true);
  }

  const closeAbout = () => {
    setAboutOpen(false);
  }

  return (
    <div>
      <header className="App-header">
        <AppBar color="secondary" position="static">
          <Toolbar>
            <IconButton edge="start" className={classes.menuButton} color="inherit" onClick={openAbout}>
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              ORANGE ENGINE
            </Typography>
            <div className={classes.user}>
              <Login user={user} onSetUser={setUser} />
            </div>
          </Toolbar>
        </AppBar>
        <About isOpen={aboutOpen} onClose={closeAbout} />
      </header>

        {(error !== null) && (
          <div>
            <p className={classes.error}>Er is iets mis met de server waar dit programma op draait. Probeer het later nog eens.</p>
            <p className={classes.error}>{error}</p>
          </div>
        )}

        {<Ideas onError={setError} user={user} />}

    </div>
  );
}

export default App;
