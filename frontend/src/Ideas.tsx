import React, { useEffect, useState } from "react";
import { DataGrid, ColDef, SortDirection, ValueFormatterParams } from '@material-ui/data-grid';
import { makeStyles, Theme } from "@material-ui/core/styles";
import AddIdea from "./AddIdea";
import { User } from "./App";

import config from "./config.json";

type onErrorHandler = (message: string) => void;

interface IdeasProperties {
    user: User;
    onError: onErrorHandler;
}

const useStyles = makeStyles((theme: Theme) => ({
    ideas: {
        margin: theme.spacing(1)
    },
    datagrid: {
        color: theme.palette.text.secondary
    },
    link: {
        color: theme.palette.text.secondary
    }
}));

export interface IdeaItem {
    id: number;
    datetime: string;
    username: string;
    platformname: string;
    transactionhash: string;
    ideaname: string;
    comments: string;
}

function Ideas(props: IdeasProperties) {

  const classes = useStyles();

  const [busy, setBusy] = useState<boolean>(false);
  const [ideas, setIdeas] = useState<IdeaItem[]>([]);
  const [reloadCount, setReloadCount] = useState<number>(0);
  
  useEffect(() => {
        setBusy(true);
        const headers = ({
        "Accept": "application/json",
        "Content-type": "application/json"
        });
        fetch(config.urlIdeas, {
            mode: "cors",
            headers: headers,
            method: "get"
        })
        .then(result => {
            if (result.status === 200) {
                return result.json();
            } else {
                console.log(result);
                throw new Error("REST service not okay. " + result.status + ": " + result.statusText);
            }
        })
        .then(json => {

                let newIdeas: IdeaItem[] = [];

                // Parse results from days API.
                for (let record of json) {
                    const row:IdeaItem = {
                      id: record.ideaid,
                      datetime: record.datetime,
                      username: record.username,
                      platformname: record.platformname,
                      transactionhash: record.transactionhash,
                      ideaname: record.ideaname,
                      comments: record.comments,
                    };
                    newIdeas.push(row);
                }
                setIdeas(newIdeas);

        })
        .catch(error => {
            console.log(error.message);
            //restError(error.message);
        })
        .finally(() => {
            setBusy(false);
        });
  }, [reloadCount]);

  // By incrementing the reload count, useEffect is triggered and the contents will be reloaded.
  const reloadRequest = () => {
      setReloadCount(reloadCount + 1);
  }

  const columns: ColDef[] = [
    { field: "id", headerName: "id", width: 70 },
    { field: "datetime", headerName: "Datum", width: 180},
    { field: "username", headerName: "Auteur", width: 120},
    { field: "platformname", headerName: "Platform", width: 160},
    { field: "ideaname", headerName: "Naam", width: 160},
    { field: "comments", headerName: "Opmerkingen", width: 380},
    { field: "transactionhash", headerName: "Transactie", width: 820,
      renderCell: (params: ValueFormatterParams) =>
       { 
           const url:string = "https://explorer.iota.org/devnet/transaction/" + params.value;
           return (<a className={classes.link} href={url} target="_blank" rel="noreferrer">{params.value}</a>); }
       }
  ];


return (
      <section className={classes.ideas}>

        <div style={{ height: 800, width: '100%' }}>
            <DataGrid
            className={classes.datagrid}
            loading={busy}
            rows={ideas} 
            columns={columns} 
            autoPageSize
            sortModel={[
                {
                    field: 'id',
                    sort: 'asc' as SortDirection,
                }]}
            />
        </div>

        <AddIdea user={props.user} onChanged={reloadRequest}/>

      </section>
  );
}

export default Ideas;