ALTER TABLE `platform` ADD `geturl` VARCHAR(256);

UPDATE `platform` SET `geturl` = 'https://explorer.iota.org/devnet/transaction/{}' WHERE `platformid` = 1;

INSERT INTO `idea` (`datetime`, `userid`, `platformid`, `transactionhash`, `ideaname`, `comments`) VALUES
('2021-02-04 12:13', 1, 1, 'JPDYGXL99LVIVPCUYROCGOTLFGNW9A9XBILODYAFAKYTSWAISSBFKIBNLXMMHPIMWUZEZRJQPEJPRZ999', 'Second test', 'Cynical'),
('2021-02-04 12:17', 1, 1, 'KFYABKYAAYEQROXKUVCQVAMNLJZDWESTUKCWCORNWFXBGZTKNMKI9LSFTRAWOJQUEGDQQPJVYHJSVJ999', 'Third test', 'Brand new day');
