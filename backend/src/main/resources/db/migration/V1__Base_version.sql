/* Create user table and insert users. */
CREATE TABLE `user` (
    `userid` INT AUTO_INCREMENT PRIMARY KEY, 
    `username` VARCHAR(64),
    `realname` VARCHAR(64),
    `role` VARCHAR(64),
    `email` VARCHAR(128), 
    `password` VARCHAR(64),
    `token` VARCHAR(48),
    `tokenexpirytime` DATETIME
);

INSERT INTO `user` (`userid`, `username`, `realname`, `role`, `email`, `password`) VALUES (1, 'paul', 'Paul', 'admin', 'paul@twisq.nl', 'BunkerHok8');
INSERT INTO `user` (`userid`, `username`, `realname`, `role`, `email`, `password`) VALUES (2, 'ed', 'Ed', 'admin', 'eddyrijs@gmail.com', 'NietVergeten1709');

/* Create table for platforms. */
CREATE TABLE `platform` (
    `platformid` INT AUTO_INCREMENT PRIMARY KEY,
    `platformname` VARCHAR(256),
    `url` VARCHAR(256)
);

INSERT INTO `platform` (`platformid`, `platformname`, `url`) VALUES (1, 'IOTA Devnet', 'https://nodes.devnet.iota.org:443');

/* Create table for ideas. */
CREATE TABLE `idea` (
    `ideaid` INT AUTO_INCREMENT PRIMARY KEY,
    `datetime` DATETIME,
    `userid` INT,
    `platformid` INT,
    `transactionhash` VARCHAR(256), 
    `ideaname` VARCHAR(256),
    `comments` VARCHAR(256)
);

INSERT INTO `idea` (`datetime`, `userid`, `platformid`, `transactionhash`, `ideaname`, `comments`) 
VALUES ('2021-01-30 18:13', 1, 1, 'YT9HNULKQIYKK9DYHLCYQE9LXRDYOXLCTEHNPPKCZXSCKQCVOWDTGVSUCIBVJQNIZNCUMMDXNYUGDK999', 'First test', 'Below the surface');