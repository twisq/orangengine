package nl.twisq.orangengine;

import java.util.ArrayList;

import org.iota.jota.IotaAPI;
import org.iota.jota.dto.response.SendTransferResponse;
import org.iota.jota.error.ArgumentException;
import org.iota.jota.model.Transfer;
import org.iota.jota.model.Transaction;
import org.iota.jota.utils.SeedRandomGenerator;
import org.iota.jota.utils.TrytesConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TangleService {

    public static String addIdeaToTangle(PostIdea idea) {

        Logger logger = LoggerFactory.getLogger(TangleService.class);

        IotaAPI api = new IotaAPI.Builder()
        .protocol("https")
        .host("nodes.devnet.iota.org")
        .port(443)
        .build();

        int depth = 3;
        int minimumWeightMagnitude = 9;

        String address = "ZLGVEQ9JUZZWCZXLWVNTHBDX9G9KZTJP9VEERIIFHY9SIQKYBVAHIMLHXPQVE9IXFDDXNHQINXJDRPFDXNYVAPLZAW";

        String myRandomSeed = SeedRandomGenerator.generateNewSeed();

        int securityLevel = 2;

        // TODO: Remove illegal ASCII characters.
        String message = TrytesConverter.asciiToTrytes(idea.getIdea());
        String tag = "ORANGE";
        
        int value = 0;
        Transfer zeroValueTransaction = new Transfer(address, value, message, tag);
        ArrayList<Transfer> transfers = new ArrayList<Transfer>();
        transfers.add(zeroValueTransaction);

        String hash = null;
        try { 
            SendTransferResponse response = api.sendTransfer(myRandomSeed, securityLevel, depth, minimumWeightMagnitude, transfers, null, null, false, false, null);
            for (Transaction transaction : response.getTransactions()) {
                hash = transaction.getHash();
                logger.info("Hash = {}", hash);
            }
            return hash;
        } catch (ArgumentException e) { 
            // Handle error
            e.printStackTrace(); 
        }

        return hash;
	}
}