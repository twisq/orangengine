package nl.twisq.orangengine;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Collection;
import java.util.UUID;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Instant;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@CrossOrigin
@RestController
public class OrangEngineController {

	Logger logger = LoggerFactory.getLogger(OrangEngineController.class);

	@Autowired
	private IdeaRepository ideaRepository;

	@Autowired
	private PlatformRepository platformRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	BuildProperties buildProperties;

	@RequestMapping("/info")
	public Map<String, String> index() {
		logger.info("Info requested");
		Map<String, String> response = new HashMap<>();
		response.put("application", "TWISQ OrangEngine");
		response.put("version", buildProperties.getVersion());
		response.put("buildAt", buildProperties.getTime().toString());
		return response;
	}

	@PostMapping("/login")
	public Map<String, String> login(@RequestBody PostUser candidate) {

		// Check if user has entered valid credentials.
		for (User registratedUser : userRepository.findAll()) {
			if (candidate.getUsername().equals(registratedUser.getUsername()) && candidate.getPassword().equals(registratedUser.getPassword())) {

				// Credentials are valid. Create a token and return it.
				Instant expires = Instant.now().plusSeconds(3600);
				registratedUser.setToken(UUID.randomUUID().toString());
				registratedUser.setTokenexpirytime(expires);
				userRepository.save(registratedUser);
				HashMap<String, String> map = new HashMap<>();
				map.put("username", registratedUser.getUsername());
				map.put("realname", registratedUser.getRealname());
				map.put("email", registratedUser.getEmail());
				map.put("token", registratedUser.getToken());
				map.put("role", registratedUser.getRole());
				map.put("tokenexpirytime", registratedUser.getTokenexpirytime().toString());
				logger.info("User {} has logged in succesfully.", registratedUser.getUsername());
				return map;
			}
		}

		// No valid user found. Return 401.
		logger.info("User {} has tried to log in unsuccesfully.", candidate.getUsername());
		throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid combination of username and password.");
	}

	@PostMapping("/addidea")
	public Map<String, String> addIdea(@RequestBody PostIdea idea) {

		Map<String, String> response = new HashMap<>();

		// Look up user and check if key has expired.
		for (User user : userRepository.findAll()) {
			if ((user.getToken() != null) && (user.getToken().equals(idea.getToken()))) {
				if (user.getTokenexpirytime().isBefore(Instant.now())) {
					logger.info("Login of user {} has expired.", user.getUsername());
					throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Login has expired.");
				}
				
				// Login is valid. Now take the appropriate action.
				logger.info("User {} is adding idea {}.", user.getUsername(), idea.getName());

				String transactionhash = TangleService.addIdeaToTangle(idea);
				if (transactionhash == null) {
					logger.info("Error while adding to the Tangle.");
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot add to the Tangle.");
				} else {
					Idea newIdea = new Idea();
					newIdea.setComments(idea.getComments());
					newIdea.setDatetime(LocalDateTime.now());
					newIdea.setIdeaname(idea.getName());
					newIdea.setPlatformid(1L);
					newIdea.setTransactionhash(transactionhash);
					newIdea.setUserid(user.getUserid());
					ideaRepository.save(newIdea);
					response.put("transactionhash", transactionhash);
				}
			}
		}
		return response;
	}

	@GetMapping("/ideas")
	public Collection<GetIdea> ideas() {
		logger.info("Ideas requested");
		List<GetIdea> ideas = new ArrayList<>();

		// Get list of ideas.
		for (Idea idea : ideaRepository.findAll()) {
			GetIdea record = new GetIdea();
			record.setIdeaid(idea.getIdeaid());
			record.setDatetime(idea.getDatetime());
			record.setPlatformname(platformRepository.findById(idea.getPlatformid()).orElseThrow().getPlatformname());
			record.setUsername(userRepository.findById(idea.getUserid()).orElseThrow().getUsername());
			record.setTransactionhash(idea.getTransactionhash());
			record.setIdeaname(idea.getIdeaname());
			record.setComments(idea.getComments());
			ideas.add(record);
		}

		return ideas;
	}

	@GetMapping("/platforms")
	public Collection<Platform> platforms() {
		logger.info("Platforms requested");
		List<Platform> platforms = new ArrayList<>();

		// Get list of platforms.
		for (Platform platform : platformRepository.findAll()) {
			platforms.add(platform);
		}
		return platforms;
	}

/*	private void createCalendarRecord(Map<LocalDate, CalendarItem> calendar, Availability availability, String realname) {
		// Create a new record with only the availability of this user.
		CalendarItem record = new CalendarItem();
		record.setDate(availability.getDate());
		record.setRehearsal(false);
		if (availability.isAvailable()) {
			record.setAvailable(realname);
		} else {
			record.setNotavailable(realname);
		}
		record.setFromtime(availability.getFromtime());
		record.setTotime(availability.getTotime());
		record.setComments(availability.getComments());
		calendar.put(record.getDate(), record);
	}

	private void updateCalendarRecord(Map<LocalDate, CalendarItem> calendar, Availability availability, String realname) {
		// Update availability to existing record.
		CalendarItem record = calendar.get(availability.getDate());
		if (availability.isAvailable()) {
			record.setAvailable(addStringToCommalist(record.getAvailable(), realname));
		} else {
			record.setNotavailable(addStringToCommalist(record.getNotavailable(), realname));
		}

		// Only update fromtime, totime and comments if it is not a rehearsal.
		if (!record.isRehearsal()) {
			if (availability.getFromtime() != null && (record.getFromtime() == null || availability.getFromtime().isAfter(record.getFromtime()))) {
				record.setFromtime(availability.getFromtime());
			}
			if (availability.getTotime() != null && (record.getTotime() == null || availability.getTotime().isBefore(record.getTotime()))) {
				record.setTotime(availability.getTotime());
			}
			record.setComments(addStringToCommalist(record.getComments(), availability.getComments()));
		}
		calendar.put(record.getDate(), record);
	}

	// Clean up the database and delete old events every night at 4AM.
	@Scheduled(cron="0 0 4 * * ?")
	public void deleteOldRecords() {
		int count = 0;
		for (Rehearsal rehearsal : rehearsalRepository.findAll()) {
			if (rehearsal.getDate().isBefore(LocalDate.now())) {
				rehearsalRepository.delete(rehearsal);
				count++;
			}
		}
		for (Availability availability : availabilityRepository.findAll()) {
			if (availability.getDate().isBefore(LocalDate.now())) {
				availabilityRepository.delete(availability);
				count++;
			}
		}
		logger.info("Database cleaned. {} records removed.", count);
	}

	@RequestMapping("/plan")
	public Map<String, String> plan(@RequestBody PostAction action) {
		// Look up user and check if key has expired.
		for (User user : userRepository.findAll()) {
			if ((user.getToken() != null) && (user.getToken().equals(action.getToken()))) {
				if (user.getTokenexpirytime().isBefore(Instant.now())) {
					logger.info("Login of user {} has expired.", user.getUsername());
					throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Login has expired.");
				}
				
				// Login is valid. Now take the appropriate action.
				logger.info("User {} requests action {} on {} number of dates.", user.getUsername(), action.getAction(), action.getDates().size());
				Long userid = user.getUserid();
				if (action.getAction().equals("SetAvailable")) {
					executeSetAvailable(action, userid);
				} else if (action.getAction().equals("SetNotAvailable")) {
					executeSetNotAvailable(action, userid);
				} else if (action.getAction().equals("SetRehearse")) {
					executeSetRehearse(action);
				} else if (action.getAction().equals("SetNotRehearse")) {
					executeSetNotRehearse(action);
				} else {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unknown action.");
				}

				Map<String, String> response = new HashMap<>();
				response.put("username", user.getUsername());
				response.put("status", "OK");	
				return response;
			}
		}
		throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "No valid login.");
	}

	private void executeSetNotRehearse(PostAction action) {
		for (Rehearsal rehearsal : rehearsalRepository.findAll()) {
			if (action.getDates().contains(rehearsal.getDate())) {
				rehearsalRepository.delete(rehearsal);
			}
		}
	}

	private void executeSetRehearse(PostAction action) {
		for (LocalDate datetoset : action.getDates()) {
			boolean done = false;
			for (Rehearsal rehearsal : rehearsalRepository.findAll()) {
				if (datetoset.isEqual(rehearsal.getDate())) {
					rehearsal.setFromtime(action.getFromtime());
					rehearsal.setTotime(action.getTotime());
					rehearsal.setComments(action.getComments());
					rehearsalRepository.save(rehearsal);
					done = true;
				}
			}	
			if (!done) {
				// Make a new record.
				Rehearsal rehearsal = new Rehearsal();
				rehearsal.setDate(datetoset);
				rehearsal.setFromtime(action.getFromtime());
				rehearsal.setTotime(action.getTotime());
				rehearsal.setComments(action.getComments());
				rehearsalRepository.save(rehearsal);
			}
		}
	}

	private void executeSetNotAvailable(PostAction action, Long userid) {
		for (LocalDate datetoset : action.getDates()) {
			boolean done = false;
			for (Availability available : availabilityRepository.findAll()) {
				if (datetoset.isEqual(available.getDate()) && available.getUserid().equals(userid)) {
					available.setAvailable(false);
					available.setComments(action.getComments());
					availabilityRepository.save(available);
					done = true;
				}
			}	
			if (!done) {
				// Create a new record.
				Availability newavailable = new Availability();
				newavailable.setDate(datetoset);
				newavailable.setUserid(userid);
				newavailable.setAvailable(false);
				newavailable.setComments(action.getComments());
				availabilityRepository.save(newavailable);
			}
		}
	}

	private void executeSetAvailable(PostAction action, Long userid) {
		for (LocalDate datetoset : action.getDates()) {
			boolean done = false;
			for (Availability availability : availabilityRepository.findAll()) {
				if (datetoset.isEqual(availability.getDate()) && availability.getUserid().equals(userid)) {
					availability.setAvailable(true);
					availability.setFromtime(action.getFromtime());
					availability.setTotime(action.getTotime());
					availability.setComments(action.getComments());
					availabilityRepository.save(availability);
					done = true;
				}
			}	
			if (!done) {
				// Create a new record.
				Availability newavailable = new Availability();
				newavailable.setDate(datetoset);
				newavailable.setUserid(userid);
				newavailable.setAvailable(true);
				newavailable.setFromtime(action.getFromtime());
				newavailable.setTotime(action.getTotime());
				newavailable.setComments(action.getComments());
				availabilityRepository.save(newavailable);
			}
		}
	}
*/
}