package nl.twisq.orangengine;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalTime;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostIdea {
    private String token;
    private String platform;
    private String name;
    private String comments;
    private String idea;
}
