package nl.twisq.orangengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class OrangEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrangEngineApplication.class, args);
	}
	
}
