package nl.twisq.orangengine;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Id;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetIdea {

    private Long ideaid;
    private LocalDateTime datetime;
    private String username;
    private String platformname;
    private String transactionhash;
    private String ideaname;
    private String comments;
}
