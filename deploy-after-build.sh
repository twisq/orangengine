cd frontend
yarn build
cd ../backend
mvn clean package
cd ../deploy
ansible-playbook -i hosts orangengine.yml
